﻿using ProductManagerment_EFCore.Entities.RelationEntities;

namespace ProductManagerment_EFCore.IRepository
{
    public interface IProductOrderRepository : IGenericRepository<ProductOrder>
    {
    }
}
