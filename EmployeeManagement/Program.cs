﻿using EmployeeManagement;
using EmployeeManagement.Entities;
using EmployeeManagement.IRepositories;
using EmployeeManagement.Repository;

// create instance
var context = new AppDbContext();
IEmployeeRepository employeeRepository = new EmployeeRepository(context);
IProjectRepositry projectRepositry = new ProjectRepository(context);

// seeding data
if (!context.Set<EmployeeProject>().Any())
{
    var listData = new List<EmployeeProject>()
    {
        new EmployeeProject()
        {
            Employee = new Employee()
            {
                Email = "tuandung@gmail.com",
                PhoneNumber = "12345",
            },
            Project = new Project()
            {
                Name = "DotNetCore#1",
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(3),
            }
        },
        new EmployeeProject()
        {
            Employee = new Employee()
            {
                Email = "dung@gmail.com",
                PhoneNumber = "12345123",
            },
            Project = new Project()
            {
                Name = "DotNetCore#2",
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(3),
            }
        },
        new EmployeeProject()
        {
            Employee = new Employee()
            {
                Email = "dungtuan@gmail.com",
                PhoneNumber = "1234567",
            },
            Project = new Project()
            {
                Name = "DotNetCore#3",
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(3),
            }
        }
    };

    await context.Set<EmployeeProject>().AddRangeAsync(listData);
    await context.SaveChangesAsync();
};

//
var listEmp = await employeeRepository.Paging(0, 10);
foreach (var item in listEmp)
{
    Console.WriteLine(item.Email);
}

var emp = await employeeRepository.GetEmployeeAsync(1);
var pro = emp.EmployeeProjects.Select(x => x.Project).ToList();
foreach (var item in pro)
{
    Console.WriteLine(item.Name);
}