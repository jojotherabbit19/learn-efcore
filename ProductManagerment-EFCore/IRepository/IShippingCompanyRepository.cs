﻿using ProductManagerment_EFCore.Entities;

namespace ProductManagerment_EFCore.IRepository
{
    public interface IShippingCompanyRepository : IGenericRepository<ShippingCompany>
    {
    }
}
