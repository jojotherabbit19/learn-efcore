﻿using EmployeeManagement.Entities;
using EmployeeManagement.IRepositories;
using Microsoft.EntityFrameworkCore;

namespace EmployeeManagement.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseEntity
    {
        protected DbSet<T> _dbSet;
        private readonly AppDbContext _appDbContext;
        public GenericRepository(AppDbContext context)
        {
            _appDbContext = context;
            _dbSet = context.Set<T>();
        }
        public async Task AddAsync(T entity)
        {
            await _dbSet.AddAsync(entity);
            await _appDbContext.SaveChangesAsync();
        }

        public async void Delete(T entity)
        {
            entity.IsDelete = true;
            _dbSet.Update(entity);
            await _appDbContext.SaveChangesAsync();
        }

        public async void Update(T entity)
        {
            _dbSet.Update(entity);
            await _appDbContext.SaveChangesAsync();
        }

        public async Task<List<T>> Paging(int page = 0, int pageSize = 10)
        {
            var result = await _dbSet.Skip(page * pageSize)
                                     .Take(pageSize)
                                     .AsNoTracking()
                                     .ToListAsync();
            return result;
        }
    }
}
