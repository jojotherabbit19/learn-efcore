﻿using EFCoreRelationship;
using Microsoft.EntityFrameworkCore;

class GenericRepository<T> : IGenericRepository<T> where T : class
{
    private readonly AppDbContext _appDbContext;
    public GenericRepository(AppDbContext appDbContext)
    {
        _appDbContext = appDbContext;
    }

    public async Task AddAsync(T entity)
    {
        await _appDbContext.AddAsync(entity);
        await _appDbContext.SaveChangesAsync();
    }

    public async Task<List<T>> DisplayAsync()
    {
        var entities = await _appDbContext.Set<T>().ToListAsync();

        return entities;
    }
}