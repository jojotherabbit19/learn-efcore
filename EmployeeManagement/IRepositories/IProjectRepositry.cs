﻿using EmployeeManagement.Entities;

namespace EmployeeManagement.IRepositories
{
    public interface IProjectRepositry : IGenericRepository<Project>
    {
    }
}
