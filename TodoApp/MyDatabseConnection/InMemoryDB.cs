﻿using Microsoft.EntityFrameworkCore;
using TodoApp.Entities;

namespace TodoApp.MyDatabseConnection
{
    public class InMemoryDB : DbContext
    {
        public DbSet<Todo> Todos { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseInMemoryDatabase("myDB");
        }
    }
}
