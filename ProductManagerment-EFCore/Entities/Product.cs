﻿using ProductManagerment_EFCore.Entities.RelationEntities;

namespace ProductManagerment_EFCore.Entities
{
    public class Product : BaseEntity
    {
        public string Name { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public virtual ICollection<ProductOrder> ProductOrders { get; set; }
    }
}
