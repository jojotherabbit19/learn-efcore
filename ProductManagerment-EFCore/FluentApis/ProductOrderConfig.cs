﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProductManagerment_EFCore.Entities.RelationEntities;

namespace ProductManagerment_EFCore.FluentApis
{
    public class ProductOrderConfig : IEntityTypeConfiguration<ProductOrder>
    {
        public void Configure(EntityTypeBuilder<ProductOrder> builder)
        {
            //ignore id key from base
            builder.Ignore(x => x.Id);
            //create new PK
            builder.HasKey(x => new { x.OrderId, x.ProductId });
            //
            builder.HasOne(x => x.Product)
                   .WithMany(x => x.ProductOrders)
                   .HasForeignKey(x => x.ProductId);

            builder.HasOne(x => x.Order)
                   .WithMany(x => x.ProductOrders)
                   .HasForeignKey(x => x.OrderId);
        }
    }
}
