﻿using ProductManagerment_EFCore.Entities.RelationEntities;
using ProductManagerment_EFCore.IRepository;

namespace ProductManagerment_EFCore.Repository
{
    public class ProductOrderRepository : GenericRepository<ProductOrder>, IProductOrderRepository
    {
        public ProductOrderRepository(AppDbContext dbContext) : base(dbContext)
        {
        }
    }
}
