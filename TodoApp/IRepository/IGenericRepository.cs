﻿using TodoApp.Entities;

namespace TodoApp.IRepository
{
    public interface IGenericRepository<T> where T : BaseEntity
    {
        public void Add(T entity);
        public void Delete(int Id);
        public List<T> View();
    }
}
