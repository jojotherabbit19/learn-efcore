﻿namespace EmployeeManagement.Entities
{
    public class Employee : BaseEntity
    {
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public virtual ICollection<EmployeeProject>? EmployeeProjects { get; set; }
    }
}
