﻿using TodoApp.Entities;
using TodoApp.IRepository;

namespace TodoApp.Repository
{
    public class ListGenericRepository<T> : IGenericRepository<T> where T : BaseEntity
    {
        List<T> list = new List<T>();

        public void Add(T entity)
        {
            list.Add(entity);
        }

        public void Delete(int Id)
        {
            var obj = list.FirstOrDefault(x => x.Id == Id);
            if (obj != null)
            {
                list.Remove(obj);
            }
            else
            {
                throw new Exception();
            }
        }

        public List<T> View() => list;
    }
}
