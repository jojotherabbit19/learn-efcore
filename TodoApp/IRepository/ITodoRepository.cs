﻿using TodoApp.Entities;

namespace TodoApp.IRepository
{
    public interface ITodoRepository : IGenericRepository<Todo>
    {

    }
}
