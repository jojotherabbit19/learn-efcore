﻿using Microsoft.EntityFrameworkCore;
using ProductManagerment_EFCore.IRepository;

namespace ProductManagerment_EFCore.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseEntity
    {
        private readonly AppDbContext _dbContext;
        protected readonly DbSet<T> _dbSet;
        public GenericRepository(AppDbContext dbContext)
        {
            _dbContext = dbContext;
            _dbSet = dbContext.Set<T>();
        }

        public async Task CreateAsync(T entity)
        {
            await _dbSet.AddAsync(entity);
        }

        public async Task<List<T>> GetAllAsync(int page = 0, int pageSize = 10)
        {
            var result = await _dbSet.Skip(page * pageSize)
                               .Take(pageSize)
                               .AsNoTracking()
                               .ToListAsync();

            return result;
        }

        public void SoftDelete(T entity)
        {
            entity.IsDelete = true;
            _dbSet.Update(entity);
        }

        public void Update(T entity)
        {
            _dbSet.Update(entity);
        }
    }
}
