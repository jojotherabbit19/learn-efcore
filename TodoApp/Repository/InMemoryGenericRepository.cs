﻿using TodoApp.Entities;
using TodoApp.IRepository;
using TodoApp.MyDatabseConnection;

namespace TodoApp.Repository
{
    public class InMemoryGenericRepository<T> : IGenericRepository<T> where T : BaseEntity
    {
        InMemoryDB db = new InMemoryDB();
        public void Add(T entity)
        {
            db.Set<T>().Add(entity);
            db.SaveChanges();
        }

        public void Delete(int Id)
        {
            var obj = db.Set<T>().FirstOrDefault(x => x.Id == Id);
            if (obj != null)
            {
                db.Set<T>().Remove(obj);
                db.SaveChanges();
            }
            else
            {
                throw new Exception();
            }
        }

        public List<T> View() => db.Set<T>().ToList();
    }
}
