﻿namespace ProductManagerment_EFCore.IRepository
{
    public interface IGenericRepository<T> where T : BaseEntity
    {
        Task CreateAsync(T entity);
        void Update(T entity);
        void SoftDelete(T entity);
        Task<List<T>> GetAllAsync(int page = 0, int pageSize = 10);
    }
}
