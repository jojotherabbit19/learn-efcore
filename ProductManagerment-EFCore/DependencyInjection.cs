﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ProductManagerment_EFCore.IRepository;
using ProductManagerment_EFCore.Repository;

namespace ProductManagerment_EFCore
{
    public static class DependencyInjection
    {
        public static IServiceCollection MyServices(IServiceCollection services)
        {
            services.AddScoped<ICustomerRepository, CustomerRepository>();
            services.AddScoped<IOrderRepository, OrderRepository>();
            services.AddScoped<IProductOrderRepository, ProductOrderRepository>();
            services.AddScoped<IShippingCompanyRepository, ShippingCompanyRepository>();
            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddDbContext<AppDbContext>(option => option.UseSqlServer("Data Source=JOJO\\JOJO;Initial Catalog=ProductDB;User ID=sa;Password=123;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False")
                      .UseLazyLoadingProxies());

            return services;
        }
    }
}
