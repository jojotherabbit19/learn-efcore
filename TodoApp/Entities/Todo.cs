﻿namespace TodoApp.Entities
{
    public class Todo : BaseEntity
    {
        public string Content { get; set; }
    }
}
