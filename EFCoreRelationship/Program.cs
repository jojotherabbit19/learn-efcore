﻿using EFCoreRelationship;

AppDbContext appDbContext = new AppDbContext();
IGenericRepository<Customer> repository = new GenericRepository<Customer>(appDbContext);

await repository.AddAsync(new Customer()
{
    Name = "Tuan Dung",
    Address = new Address() { Detail = "HoChiMinh" },
});

var cusList = await repository.DisplayAsync();
foreach (var item in cusList)
{
    Console.WriteLine(item.Name);
}