﻿using ProductManagerment_EFCore.Entities;
using ProductManagerment_EFCore.IRepository;

namespace ProductManagerment_EFCore.Repository
{
    public class ShippingCompanyRepository : GenericRepository<ShippingCompany>, IShippingCompanyRepository
    {
        public ShippingCompanyRepository(AppDbContext dbContext) : base(dbContext)
        {
        }
    }
}
