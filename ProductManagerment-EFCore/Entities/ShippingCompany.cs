﻿namespace ProductManagerment_EFCore.Entities
{
    public class ShippingCompany : BaseEntity
    {
        public string Name { get; set; }
        public virtual ICollection<Order>? Orders { get; set; }
    }
}
