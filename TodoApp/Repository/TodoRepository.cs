﻿using TodoApp.Entities;
using TodoApp.IRepository;

namespace TodoApp.Repository
{
    public class TodoRepository : ListGenericRepository<Todo>, ITodoRepository
    {
    }
}
