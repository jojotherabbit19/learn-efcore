﻿interface IGenericRepository<T> where T : class
{
    public Task AddAsync(T entity);
    public Task<List<T>> DisplayAsync();
}
