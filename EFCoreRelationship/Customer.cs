﻿using EFCoreRelationship;

public class Customer
{
    public int Id { get; set; }
    public string Name { get; set; }
    public Address Address { get; set; }
    public ICollection<Order>? Orders { get; set; }
}
