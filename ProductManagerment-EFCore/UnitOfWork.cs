﻿using ProductManagerment_EFCore.IRepository;
using ProductManagerment_EFCore.Repository;

namespace ProductManagerment_EFCore
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _dbContext;
        public UnitOfWork(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public ICustomerRepository CustomerRepository => new CustomerRepository(_dbContext);

        public IOrderRepository OrderRepository => new OrderRepository(_dbContext);

        public IProductRepository ProductRepository => new ProductRepository(_dbContext);

        public IProductOrderRepository ProductOrderRepository => new ProductOrderRepository(_dbContext);

        public IShippingCompanyRepository ShippingCompanyRepository => new ShippingCompanyRepository(_dbContext);

        public async Task SaveChangeAsync()
        {
            await _dbContext.SaveChangesAsync();
        }
    }
}
