﻿using EmployeeManagement.Entities;

namespace EmployeeManagement.IRepositories
{
    public interface IGenericRepository<T> where T : BaseEntity
    {
        Task AddAsync(T entity);
        void Update(T entity);
        void Delete(T entity);
        Task<List<T>> Paging(int page = 0, int pageSize = 10);
    }
}
