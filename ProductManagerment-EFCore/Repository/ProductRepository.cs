﻿using ProductManagerment_EFCore.Entities;
using ProductManagerment_EFCore.IRepository;

namespace ProductManagerment_EFCore.Repository
{
    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        public ProductRepository(AppDbContext dbContext) : base(dbContext)
        {
        }
    }
}
