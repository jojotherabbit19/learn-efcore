﻿using ProductManagerment_EFCore.Entities.RelationEntities;

namespace ProductManagerment_EFCore.Entities
{
    public class Order : BaseEntity
    {
        public DateTime OrderDate { get; set; }
        public decimal TotalPrice { get; set; }
        // navigation property
        public int CustomerId { get; set; }
        public virtual Customer Customer { get; set; }
        public int ShippingCompanyId { get; set; }
        public virtual ShippingCompany ShippingCompany { get; set; }
        public virtual ICollection<ProductOrder>? ProductOrders { get; set; }
    }
}
