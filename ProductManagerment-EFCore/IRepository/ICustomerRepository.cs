﻿using ProductManagerment_EFCore.Entities;

namespace ProductManagerment_EFCore.IRepository
{
    public interface ICustomerRepository : IGenericRepository<Customer>
    {
    }
}
