﻿using EmployeeManagement.Entities;
using EmployeeManagement.IRepositories;

namespace EmployeeManagement.Repository
{
    public class ProjectRepository : GenericRepository<Project>, IProjectRepositry
    {
        public ProjectRepository(AppDbContext context) : base(context)
        {
        }
    }
}
