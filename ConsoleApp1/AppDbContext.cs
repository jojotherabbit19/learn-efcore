﻿using Microsoft.EntityFrameworkCore;

namespace ConsoleApp1
{
    public class AppDbContext : DbContext
    {
        //public AppDbContext(DbContextOptions<AppDbContext> option) : base(option)
        //{

        //}

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionString = "Server=127.0.0.1;Port=5432;Database=myDataBase;User Id=postgres;Password=12345;";
            optionsBuilder.UseNpgsql(connectionString);
        }
        public DbSet<Person> Persons { get; set; }
    }
}
