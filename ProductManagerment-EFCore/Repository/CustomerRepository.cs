﻿using Microsoft.EntityFrameworkCore;
using ProductManagerment_EFCore.Entities;
using ProductManagerment_EFCore.IRepository;

namespace ProductManagerment_EFCore.Repository
{
    public class CustomerRepository : GenericRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(AppDbContext dbContext) : base(dbContext)
        {
        }
    }
}
