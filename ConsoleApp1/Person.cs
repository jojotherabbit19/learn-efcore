﻿using System.ComponentModel.DataAnnotations;

public class Person
{
    [Key]
    public int Id { get; set; }
    [Required]
    public string Name { get; set; }
    [Required]
    public string Address { get; set; }
    [Required]
    public DateTime DOB { get; set; }
    [Required]
    public bool Gender { get; set; }
}