﻿namespace EFCoreRelationship
{
    public class Product
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
