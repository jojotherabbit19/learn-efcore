﻿using Microsoft.Extensions.DependencyInjection;
using ProductManagerment_EFCore;
using ProductManagerment_EFCore.Entities;

var service = new ServiceCollection();
var myService = DependencyInjection.MyServices(service).BuildServiceProvider();
var unitOfWorkService = myService.GetService<IUnitOfWork>();


await AddNewCustomerAsync(new Customer()
{
    Name = "Tuan Dung",
    Email = "dung@gmail.com",
    IsDelete = false
});

// add new customer
async Task AddNewCustomerAsync(Customer customer)
{
    await unitOfWorkService.CustomerRepository.CreateAsync(customer);
    await unitOfWorkService.SaveChangeAsync();
}

