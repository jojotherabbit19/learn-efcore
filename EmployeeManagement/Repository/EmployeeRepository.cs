﻿using EmployeeManagement.Entities;
using EmployeeManagement.IRepositories;
using Microsoft.EntityFrameworkCore;

namespace EmployeeManagement.Repository
{
    public class EmployeeRepository : GenericRepository<Employee>, IEmployeeRepository
    {
        public EmployeeRepository(AppDbContext context) : base(context)
        {

        }

        public async Task<Employee?> GetEmployeeAsync(int id) => await _dbSet.FirstOrDefaultAsync(x => x.Id == id);
    }
}
