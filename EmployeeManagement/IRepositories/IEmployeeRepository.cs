﻿using EmployeeManagement.Entities;

namespace EmployeeManagement.IRepositories
{
    public interface IEmployeeRepository : IGenericRepository<Employee>
    {
        Task<Employee?> GetEmployeeAsync(int id);
    }
}
