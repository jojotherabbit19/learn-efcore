﻿using ProductManagerment_EFCore.IRepository;

namespace ProductManagerment_EFCore
{
    public interface IUnitOfWork
    {
        public ICustomerRepository CustomerRepository { get; }
        public IOrderRepository OrderRepository { get; }
        public IProductRepository ProductRepository { get; }
        public IProductOrderRepository ProductOrderRepository { get; }
        public IShippingCompanyRepository ShippingCompanyRepository { get; }
        Task SaveChangeAsync();
    }
}
