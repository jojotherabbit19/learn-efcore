﻿using TodoApp.Entities;
using TodoApp.IRepository;
using TodoApp.Repository;

namespace TodoApp
{
    public class BussinessLogic
    {
        ITodoRepository repo = new TodoRepository();
        public void Add()
        {
            Console.WriteLine("Enter Id: ");
            var id = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter content: ");
            var content = Console.ReadLine();
            var todo = new Todo() { Id = id, Content = content };
            repo.Add(todo);
        }
        public void Delete()
        {
            Console.WriteLine("Enter Id: ");
            var id = int.Parse(Console.ReadLine());
            repo.Delete(id);
        }
        public void View()
        {
            var list = repo.View();
            foreach (var item in list)
            {
                Console.WriteLine($"{item.Id} : {item.Content}");
            }
        }
    }
}
